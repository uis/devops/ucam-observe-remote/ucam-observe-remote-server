# Changelog

## [0.2.3](https://gitlab.developers.cam.ac.uk/uis/devops/ucam-observe/ucam-observe-remote-server/compare/0.2.2...0.2.3) (2024-09-20)

## [0.2.2](https://gitlab.developers.cam.ac.uk/uis/devops/ucam-observe/ucam-observe-remote-server/compare/0.2.1...0.2.2) (2024-09-20)


### Bug Fixes

* use reference to base pipeline job ([1fce726](https://gitlab.developers.cam.ac.uk/uis/devops/ucam-observe/ucam-observe-remote-server/commit/1fce726ba42c7aab8c6435fc32e30c86fb92acf4))

## [0.2.1](https://gitlab.developers.cam.ac.uk/uis/devops/ucam-observe/ucam-observe-remote-server/compare/0.2.0...0.2.1) (2024-09-20)


### Bug Fixes

* correct gitlab pipelines ([fb5e00e](https://gitlab.developers.cam.ac.uk/uis/devops/ucam-observe/ucam-observe-remote-server/commit/fb5e00e37a75021e2845009f4328d3871987b755))

## [0.2.0](https://gitlab.developers.cam.ac.uk/uis/devops/ucam-observe/ucam-observe-remote-server/compare/0.1.1...0.2.0) (2024-09-19)


### Features

* add public release to repository ([f519b2d](https://gitlab.developers.cam.ac.uk/uis/devops/ucam-observe/ucam-observe-remote-server/commit/f519b2dd9ed181e9fe1f1df7c80946e86b45e5c1))


### Bug Fixes

* enable release-it ci jobs ([a2b00cc](https://gitlab.developers.cam.ac.uk/uis/devops/ucam-observe/ucam-observe-remote-server/commit/a2b00cc65cb68d16e13ec9690350ac3d14fa277c))

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.2] - 2024-09-12

### Added
- CORS support

### Updated
- Updated port to 8200 for development

## [0.1.1] - 2024-05-21

### Added
- Configuration capability using `pydantic-settings`
- Rate limiting using `slowapi`
- Client id header checking

## [0.1.0] - 2024-05-20

### Added
- Initialised project
- Added /log endpoint to receive remote log information
