# USE_LIMITERS specifies whether the server should use internal rate-limiting. The default is True
# USE_LIMITERS=True

# This is the max number of requests a client (identified by their IP address) can make per minute
# if USE_LIMITERS is True. The default is 500.
# MAX_REQUESTS_PER_MINUTE=500

# This is the max number of requests that the server will accept per day if USE_LIMITERS is True.
# MAX_GLOBAL_REQUESTS_PER_DAY=100000

# This is the URI of the storage server for the rate limiter. For acceptable values of the URI see
# https://limits.readthedocs.io/en/stable/storage.html
# Required setting.
LIMIT_STORAGE=redis://cache:6379/

# If CLIENT_IDS is set clients must provide a valid client id with each request, see details of
# use under the 'Client Ids' in README.md.
# Required setting.
CLIENT_IDS ='["client-id-1", "client-id-2"]'

# The PROXY_TRUSTED_HOSTS are the remote host addresses that the server should be trusting to set
# the 'X-Forwarded-For' header. If this server is hosted behind a proxy, and that proxy is
# configured to set X-Forwarded-For, the correct client host address will be set based on the value
# of this X-Forwarded-For. This can either be a list of strings (as above for CLIENT_IDS) or a
# string.
# PROXY_TRUSTED_HOSTS = '127.0.0.1'

# The CORS_ORIGINS are the origins that the server will allow requests from. This can either be a
# list of strings (as above for CLIENT_IDS) or a string.
CORS_ORIGINS = '["http://localhost:3000"]'
