from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    limit_storage: str
    client_ids: list[str]
    use_limiters: bool = True
    max_requests_per_minute: int = 500
    max_global_requests_per_day: int = 100000
    proxy_trusted_hosts: list[str] | str = "127.0.0.1"
    cors_origins: list[str] | str = "http://localhost:3000"
