import logging
from enum import Enum
from typing import Optional

from pydantic import BaseModel, Field


class LogLevel(str, Enum):
    NOTSET = "notset"
    DEBUG = "debug"
    INFO = "info"
    WARNING = "warning"
    ERROR = "error"
    CRITICAL = "critical"

    @classmethod
    def to_logging(cls, level):
        if level == cls.NOTSET:
            return logging.NOTSET
        elif level == cls.DEBUG:
            return logging.DEBUG
        elif level == cls.INFO:
            return logging.INFO
        elif level == cls.WARNING:
            return logging.WARNING
        elif level == cls.ERROR:
            return logging.ERROR
        elif level == cls.CRITICAL:
            return logging.CRITICAL


class RemoteLog(BaseModel):
    log_level: LogLevel
    event: str
    data: Optional[dict] = Field(None)
