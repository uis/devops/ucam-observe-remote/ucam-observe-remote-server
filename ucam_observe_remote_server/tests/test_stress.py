import os

import pytest


@pytest.mark.stress
@pytest.mark.repeat(int(os.getenv("STRESS_TEST_REPEATS", "1000")))
def test_stress(stress_client):
    """
    Make a single log request to the stress_client. Repeated via pytest-repeats.
    """
    stress_client.post("/log", json={"log_level": "info", "event": "Test Event"})
